package cn.chongho.inf.flink.model;

import lombok.Data;

/**
 * @author feihu.wang
 * @since 2022-02-14
 */
@Data
public class UserDataAuthority {

    private Integer id;

    private String name;

    private String email;

    private Integer authority;



}
