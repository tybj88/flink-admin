package cn.chongho.inf.flink.mapper;

import cn.chongho.inf.flink.model.DbTable;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author ming
 */
public interface DbTableMapper extends Mapper<DbTable> {
}
